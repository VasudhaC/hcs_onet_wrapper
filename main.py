# Import the necessary packages
import logging
from flask import Flask, jsonify, abort, make_response, request
import pandas as pd 
import nltk
import pandas as pd
import snowflake.connector
from snowflake.connector import ProgrammingError
import os
import re
from collections import defaultdict
import zipfile
import json
import datetime
import jwt
from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value

app = Flask(__name__)
nltk.download("stopwords")
nltk.download('wordnet')
nltk.download('omw')
nltk.download('punkt')



"""
This section is to install the specific Model codes
"""
# Model: HCS Occupation, Experience Level Classifier, and RVT Data Lookup
from Model.occupationclassifier import OccupationModel
from Model.joblevelclassifier import LevelModel

hcs_model = OccupationModel()
level_model = LevelModel()


# Model: O*NET-SOC Occupation Classifier
from Model.jobcode_classifier import jobcodeclassifier
from Model.onetjoblevel_classifier import OnetLevelModel
#
job_code = jobcodeclassifier()
onet_level = OnetLevelModel()


# REST API usage info
@app.route('/')
def index():
    return make_response(jsonify({'method':'POST', 'path':'Call /predict for the HCS occupation, experience level, RVT rate detail, O*NET-SOC, O*NET Level'}), 200)

# setup error handling
@app.errorhandler(404)
def page_not_found(error):
    return make_response(jsonify({'error':'Not found'}), 404)


# REST API for real-time proven track record predictions.
@app.route('/predict', methods=['POST'])
def create_task():
    global updated_today
    global refreshed_date
    global df_rvt_stats
    global last_modified_date
    
    # get opportunity detail
    id = request.json.get('id', "id_unknown")
    req_job_title__c = request.json.get('req_job_title__c',"")
    req_job_description__c = request.json.get('req_job_description__c',"")

    # call all wrapper models individually
    # Model: HCS Occupation, Experience Level Classifier, and RVT Data Lookup

    # call the hcs and experience level models
    try:
        hcs, hcs_desc, hcs_prob, hcs_group = hcs_model.predict(opportunity_id=id, job_title=req_job_title__c, job_description=req_job_description__c).split(",") 
        level, level_prob = level_model.predict(job_title=req_job_title__c, job_description=req_job_description__c).split(",")
        hcs_output = {
            'status':'OK',
            'message':"Successful",
            'hcs_prob': hcs_prob,
            'hcs': hcs, 
            'hcs_group': hcs_group,
            'hcs_desc' : hcs_group,
            'level' : level ,
            'level_prob' : level_prob
    except Exception as e:
        print("HCS and/or Level Model failure: ", e)
        hcs_message = "Unsuccessful - HCS and/or Experience Level Model unable to be called"
        hcs_output = {
        'hcs' : "00-000" ,
        'hcs_desc' : "0" ,
        'hcs_prob' : "0" ,
        'hcs_group' : "0" ,
        'level' : "0" ,
        'level_prob' : "0"
        }
        


    # Model: O*NET-SOC Occupation Classifier
    onet_start = datetime.datetime.now()

    # call job code model function
    try:
        onet_output = job_code.model_predict(req_job_title__c, req_job_description__c)
        onetlevel, onetlevel_prob = onet_level.predict(job_title=req_job_title__c, job_description=req_job_description__c).split(",")
    except Exception as e:
        print("O*NET-SOC Job Code Model failure: ", e)
        onet_message = "Unsuccessful - O*NET-SOC Model unable to be completed"
        onet_output = {
            'status':'OK',
            'message':onet_message,
            'confidence': 0.0,
            'onet_soc_code':'00-0000.00', 
            'onet_soc_description':'Unknown' ,
            'onetlevel' : "0" ,
            'onetlevel_prob' : "0"
            }


    # verify job code output is available
    if onet_output:
        onet_response = onet_output
    else:
        onet_response = {
            'status':'OK',
            'message':"Unsuccessful",
            'confidence': 0.0,
            'onet_soc_code':'00-0000.00', 
            'onet_soc_description':'Unknown'
            }

    onet_end = datetime.datetime.now()
    print("onet time: ", onet_end-onet_start)


    # set up responses to return all wrapper calls
    response = {
        "status": "OK",
        "message": "Successful",
        "id": id,
        "hcs_output" : hcs_output
        "onet_output": onet_response 
    }

    return jsonify(response)

if __name__ == '__main__':
    app.run()