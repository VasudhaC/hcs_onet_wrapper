"""
This module is to provide some functions on occupation predictions
Author: Ivan Chen
Date:   04/25/2020
Modification History:
- initial creation at 04/25/2020
- updated by Alyssa Spitzer at 05/11/2020
  -- predict
  -- predict_df
"""
from __future__ import division, print_function
import sys
from os import path, walk
import pandas as pd
import numpy as np
import re
# import multiprocessing
from gensim.models import Word2Vec
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler, LabelEncoder
import joblib
from sklearn.metrics import classification_report
import logging
import warnings
import datetime

logging.getLogger().setLevel(logging.ERROR)
# disable warning message show-up
warnings.filterwarnings("ignore", category=DeprecationWarning)

dir_path = path.dirname(path.realpath(__file__))
W2V_FIle = dir_path + '/nnc_word2vec.bin'
CLASSIFIER_FILE = dir_path + '/nnc.pkl'
HCS_LABEL_FILE = dir_path + "/datasets/hcs_label_index.csv"
DIM_HCS = dir_path + "/datasets/dim_hcs_new.csv"
SCALER_FILE = dir_path + '/scaler.pkl'
# set random number seed as 42
np.random.seed(42)


# noinspection PyMethodMayBeStatic
class OccupationModel:
    """
    This class is occupation model container that provides
    - occupation prediction (real-time & batch modes)
    - occupation training process
    - stop word removal
    - lemmatization
    - word2vec conversion
    """
    def __init__(self):
        """
        Initializing some variables
        """
        try:
            self.stop_words = stopwords.words('english')
            self.model_w2v = Word2Vec.load(W2V_FIle)
            self.loaded_model = joblib.load(CLASSIFIER_FILE)
            self.hcs_label_df = pd.read_csv(HCS_LABEL_FILE, header="infer")
            self.dim_hcs = pd.read_csv(DIM_HCS, header="infer")
            self.scaler_model = joblib.load(SCALER_FILE)
        except ValueError or IOError:
            print('An error occurred trying to initialize the occupation model!')
            sys.exit()

    def get_mean_vectors(self, words: list) -> list:
        """
        This function is to calculate the mean vector values of a given list of words
        :param words: input a list of words
        :return: a list of (300) vectors which are the mean values
        """
        if words:
            words = [word for word in words if word in self.model_w2v.wv.vocab]
            if len(words) >= 1:
                return list(np.mean(self.model_w2v.wv[words], axis=0))
            else:
                return []
        else:
            return []

    def remove_stopwords(self, x: list) -> list:
        """
        This function is to return a list of words which are not stop words.
        :param x: input a list of words
        :return: a list of words which are not stop words
        """
        if x:
            return [w.lower() for w in x if (w not in self.stop_words) and (w != '') and (w is not None)]
        else:
            return []

    # noinspection PyMethodMayBeStatic,PyMethodMayBeStatic
    def lemma(self, x: list) -> list:
        """
        This function is to return a list of words which are the lemmatized ones. For example, return walk when given walking
        :param x: input a list of words
        :return: a list words which are the lemmatized ones
        """
        if x:
            lem = WordNetLemmatizer()
            return [lem.lemmatize(w.lower(), pos='v') for w in x if (w != '') and (w is not None)]
        else:
            return []

    def predict(self, opportunity_id: str, job_title: str, job_description: str) -> str:
        """
        This function is to predict an occupation with input of job title and job description
        :param opportunity_id: input opportunity id string
        :param job_title: input job title string
        :param job_description: input job description string
        :return: a string of occupation and prediction confidence value with comma delimited
        """
        # Alyssa Spitzer updated 5/8/2020 -- input values to not be null
        if opportunity_id and job_title and job_description:
            df = pd.DataFrame([[opportunity_id, job_title, job_description]], columns=['req_guid', 'job_title', 'job_description'])
            predicted = self.predict_df("", data_frame=df)
            if predicted is not None and len(predicted) > 0:
                occupation = predicted['occupation'].values[0]
                confidence = predicted['occupation_confidence'].values[0]
            else:
                occupation = "00-000"
                confidence = "0"
            # Get occupation description and occupation group
            # Alyssa Spitzer updated 5/11/2020
            # Return 0's for all HCS issues
            # when occupation = 00-000, none, or not part of dim_hcs then all values should be 0
            if occupation == "00-000" or occupation is None or occupation not in self.dim_hcs.hcs_code.values:
                occupation = "00-000"
                occupation_description = "0"
                occupation_group = "0"
                confidence = "0"
                # when no confidence is returned or not in range of 0-1, then all values should be 0 - team conversation?
            elif confidence is None or float(confidence) < 0 or float(confidence) > 1:
                occupation = "00-000"
                occupation_description = "0"
                occupation_group = "0"
                confidence = "0"
            # otherwise return values according to dim_hcs
            else:
                condition = "hcs_code == " + "'" + occupation + "'"
                occupation_info = self.dim_hcs.query(condition).head(1)
                if occupation_info.empty is False and len(occupation_info) == 1:
                    occupation_description = occupation_info['hcs_description'].to_string(index=False).strip()
                    occupation_group = occupation_info['parent_hcs_description'].to_string(index=False).strip()
                else:
                    # Alyssa Spitzer updated 5/11/2020 to include all hcs outputs
                    occupation = "00-000"
                    occupation_description = "0"
                    occupation_group = "0"
                    confidence = "0"
        else:
            occupation = "00-000"
            occupation_description = "0"
            confidence = "0"
            occupation_group = "0"
        # Return the results as a string with comma delimited
        return "".join(occupation + "," + occupation_description + "," + str(confidence) + "," + occupation_group)

    def predict_df(self, datafile: str, data_frame=None) -> pd.DataFrame:
        """
        This function is to predict occupations for a data set of jobs containing job titles and job descriptions
        :param data_frame: input pandas DataFrame object
        :param datafile: input data set file with path
        :return: a data frame with predicted occupation and prediction confidence
        """
        if data_frame is not None and len(data_frame) > 0:
            df = data_frame
        elif datafile and path.exists(datafile):
            df = pd.read_parquet(datafile, engine='pyarrow')
            # use fastparquet instead of pyarrow
            # pf = ParquetFile(datafile)
            # df = pf.to_pandas()
        else:
            df = None
        if df is not None and len(df) > 0:
            df.fillna("0")
            # make a copy to use later
            df_cache = df.copy()
            # only take necessary data columns for prediction processes
            if ('job_title' in df.columns) and ('job_description' in df.columns) and ('req_guid' in df.columns):
                df = df[['req_guid', 'job_title', 'job_description']]
                # remove punctuations
                p = re.compile("[^a-zA-Z0-9]+")
                df['job_description'] = df['job_title'].astype(str) + " " + df['job_description'].astype(str)
                df['job_description'] = df.apply(lambda row: p.sub(" ", row.job_description), axis=1)
                # Tokenize the job description and title
                # Alyssa Spitzer updated 5/8/2020 to include strip
                df['job_description'] = df["job_description"].str.lower()
                df['job_description'] = df["job_description"].str.strip(" ")
                df['job_description'] = df["job_description"].str.split(" ")
                # df['job_description'] = df["job_description"].str.split(" ")
                # Apply the defined function to remove stop words for job descriptions
                df['job_description'] = df.apply(lambda row: self.remove_stopwords(row.job_description), axis=1)
                # Apply the defined function to process job descriptions
                df['job_description'] = df.apply(lambda row: self.lemma(row.job_description), axis=1)
                # Apply the function to generate 300 dimension vectors for each job description in the data set
                df['job_description_vectors'] = df.apply(lambda row: self.get_mean_vectors(row.job_description), axis=1)
                # Alyssa Spitzer updated 5/11/2020 to filter out if list of job description vector is empty ("[]")
                df = df[(df['job_description_vectors'].astype(str) != "[]")]
                # remove any rows without job_description and this may change the index order
                df.dropna(inplace=True)
                # need to reset the index since the index will be used to concat the tables
                df = df.reset_index(drop=True)
                # check if df is not empty
                if len(df) > 0:
                    # Put job description vectors as columns
                    series = df['job_description_vectors'].apply(lambda item: np.array(item)).values.reshape(-1, 1)
                    w2v = np.apply_along_axis(lambda item: item[0], 1, series)
                    w2v_df = pd.DataFrame(w2v)
                    final_df = pd.concat([df, w2v_df], axis=1)
                    # keep vectors and remove all the other unused columns
                    final_df.drop(columns=['req_guid', 'job_title', 'job_description', 'job_description_vectors'], inplace=True)
                    # Get numpy array on feature set X
                    x = np.array(final_df)
                    # standardize the vectors
                    x = self.scaler_model.transform(x)
                    # Predict and score the data set with the loaded model
                    predicted = self.loaded_model.predict(x)
                    predicted_prob = np.amax(self.loaded_model.predict_proba(x), axis=1)
                    model_out = pd.DataFrame([(p, f) for (p, f) in zip(predicted, predicted_prob)], columns=['hcs_index', 'confidence'])
                    # Alyssa Spitzer updated on 5/11/2020
                    tmp_df = pd.concat([df, model_out], axis=1)
                    # drop unnecessary columns
                    tmp_df.drop(columns=['job_description_vectors', 'job_title', 'job_description'], inplace=True)
                    # inner join tmp_df with df_cache for all columns
                    tmp_df = pd.merge(left=df_cache, right=tmp_df, left_on='req_guid', right_on='req_guid')
                    # merge label to occupation hcs values
                    tmp_df = tmp_df.merge(self.hcs_label_df, left_on='hcs_index', right_on='label_index')
                    result_df = tmp_df.rename(columns={'label': 'occupation', 'confidence': 'occupation_confidence'})
                    # assign 00-000 and 0 to occupation and occupation_confidence if occupation are not in the list of hcs codes
                    result_df.loc[result_df.occupation.isin(self.dim_hcs.hcs_code.values) == False, ['occupation', 'occupation_confidence']] = '00-000', 0.0
                    result_df.drop(columns=['hcs_index', 'label_index'], inplace=True)
                    # print("This occupation data prediction thread has been finished.")
                    return result_df.drop_duplicates()
                else:
                    return pd.DataFrame()
            else:
                print("Data set doesn't have required columns: req_guid, job_title and job_description!")
                return pd.DataFrame()  # return an empty data frame
        else:
            return pd.DataFrame()  # return an empty data frame

    def train(self, input_file: str) -> str:
        """
        This function is to train a classifier model for occupation prediction by using neural network and word2vec
        :param input_file: input training data file with path
        :return: status
        """
        if input_file and path.exists(input_file):
            df = pd.read_parquet(input_file, engine='pyarrow')
            if ('job_title' in df.columns) and ('job_description' in df.columns) and ('req_guid' in df.columns) and ('updated_assigned_hcs' in df.columns):
                df = df[['req_guid', 'job_title', 'job_description', 'updated_assigned_hcs']]
                df.dropna(inplace=True)
                df = df.reset_index(drop=True)
                # remove punctuations
                p = re.compile("[^a-zA-Z0-9]+")
                df['job_description'] = df['job_title'].astype(str) + " " + df['job_description'].astype(str)
                df['job_description'] = df.apply(lambda row: p.sub(" ", row.job_description), axis=1)
                # Tokenize the job description and title
                df['job_description'] = df["job_description"].str.lower()
                df['job_description'] = df["job_description"].str.split(" ")
                # Apply the defined function to remove stop words for job descriptions
                df['job_description'] = df.apply(lambda row: self.remove_stopwords(row.job_description), axis=1)
                # Apply the defined function to process job descriptions
                df['job_description'] = df.apply(lambda row: self.lemma(row.job_description), axis=1)
                # Apply the function to generate 300 dimension vectors for each job description in the data set
                df['job_description_vectors'] = df.apply(lambda row: self.get_mean_vectors(row.job_description), axis=1)
                # Put job description vectors as columns
                series = df['job_description_vectors'].apply(lambda item: np.array(item)).values.reshape(-1, 1)
                w2v = np.apply_along_axis(lambda item: item[0], 1, series)
                w2v_df = pd.DataFrame(w2v)
                final_df = pd.concat([df, w2v_df], axis=1)
                final_df.rename(columns={'updated_assigned_hcs': 'label'}, inplace=True)
                final_df.drop(columns=['req_guid', 'job_title', 'job_description', 'job_description_vectors'], inplace=True)
                final_df.dropna(inplace=True)
                final_df = final_df.reset_index(drop=True)
                print("The total amount of training data is:", len(final_df))
                x = np.array(final_df.drop(columns=['label']))
                y = np.array(final_df['label'])
                # standardize the vectors
                scaler = StandardScaler()
                # fit scaler on the training data set
                scaler.fit(x)
                # save the scaler
                joblib.dump(scaler, SCALER_FILE)
                print("The scaler data have been successfully saved into the output file ", SCALER_FILE)
                x = scaler.transform(x)
                # Encoding the labels
                tmp = y
                y = LabelEncoder().fit_transform(y)
                # Save the mapping between index and labels so that I can refer back to labels when get predicted results.
                df_label = pd.DataFrame({'label': tmp, 'label_index': y})
                df_label.drop_duplicates().to_csv(HCS_LABEL_FILE, index=False, header=True)
                print("The HCS label index data have been successfully saved into the output file ", HCS_LABEL_FILE)
                # Define a neural network multi-class classifier model
                est = MLPClassifier(activation='relu', hidden_layer_sizes=(300, 200), solver='adam', learning_rate='adaptive', max_iter=500, learning_rate_init=0.001, batch_size=128, tol=0.000001, random_state=42, early_stopping=True, verbose=True, alpha=0.01)
                print("Training Neural Network MLP-Classifier...")
                nnc = est.fit(x, y)
                # Check classification (confusion matrix) report on overall data set
                print(classification_report(y, nnc.predict(x)))
                # Save the model into a file so that it can be used later on without re-training the model
                joblib.dump(nnc, CLASSIFIER_FILE)
                print("The training results have been successfully saved into the output file ", CLASSIFIER_FILE)
                return "complete"
            else:
                return "wrong input data columns"
        else:
            return "The input training data doesn't exist!"


if __name__ == "__main__":
    if len(sys.argv) == 2:
        if sys.argv[1] == "--test":
            model = OccupationModel()
            # test case 1:
            title = "Clinical Document Management Associate"
            description = "Reason for opening Department Growth and a Backfill of Daniel Caffery Must Haves 3 years experience TMF Trial management BS BA Degree The Clinical Document Management Associate is responsible for the management of the clinical study documents in the Trial Master File TMF from the development of the filing structure study start up through final archival of study documents in compliance with applicable SOPs and guidances Responsibilities Manage access and organization of the controlled clinical document storage on site at Regeneron including ensuring the defined file structure is maintained for assigned studies Scan or coordinate scanning as applicable properly name and upload documents into the eTMF Filing of essential clinical documents and maintenance of the TMF as applicable Prepare and maintain transmittals and trackers of essential documents as applicable Prepares and transitions study documents to long term archiving facility as applicable and maintain study archival records Maintain file QC schedule for assigned studies and performs periodic quality checks and inventories of study project TMFs to ensure completeness Effectively communicate and drive document management compliance and quality issues to the clinical study teams and management and offer potential solutions Support the clinical study teams in ensuring regulatory documents are submission and audit ready."
            print("Test case #1: ", model.predict("1", job_title=title, job_description=description))
            # test case 2:
            title = "Front End Developer"
            description = "Product Development - organization Pro Connect Group PCG They are looking for creative problem solvers with a passion for innovation Responsibilities Significant experience developing front end applications Significant experience developing automated tests for your features so you can get back to the fun part Hands on coding experience producing high quality experiences Resolve defects bugs during QA testing pre production production and post release patches Contribute to a scrum team focused on providing the essential benefit to customers Work with cross functional teams to ensure effective definition design and implementation Work closely with Experience Designers to understand usability information architecture Work closely with the engineering team to build the UI screens Qualifications 3 to 8 years of experience developing web applications Programming experience in several technologies including JavaScript ES6 HTML5 CSS3 Webpack Babel and SASS Working experience with frameworks and libraries such as Angular JS React JS Redux VUE jsS etc Must be able to hand code javascript Knowledge of web application development fundamentals RESTful Web Services would be a plus Knowledge JavaScript testing frameworks Test Driven Development TDD experience a plus Hands on experience creating responsive web applications using modern CSS frameworks Expert UI skills but able to understand and work in the business logic layer when needed Ability to establish priorities and work independently on multiple tasks Excellent problem solving skills Knowledge of software development methodologies and practices especially Agile iterative development processes Preferred Experience Experience developing maintaining and innovating large scale consumer facing web or mobile applications Familiar with the development challenges inherent with highly scalable and available web applications Always Be Learning Experience with open source technologies if no practical work experience with cutting edge front end technology you re prototyping and or researching the upcoming technologies and solutions Experience with various modern web frameworks Passion for creating a better user experience through understanding of usability information architecture use case development design aesthetic Experience with web analytics A B testing data visualization graphing and charting."
            print("Test case #2: ", model.predict("2", job_title=title, job_description=description))
            # test case 3:
            title = "Warehouse Material Handler - 1st Shift"
            description = "Summary Warehouse candidates will rotate weekly between the 4 primary roles: Replenishing Back end warehouse Receiving Unloading They rotate employees so that they don t get sick of doing 1 job and are always learning and doing new tasks Also it creates a more well rounded workforce and candidates feel more valuable having an array of skills Replenishing Back end 95 of the day is on one of the many types of forklift will be grabbing material and moving to requested locations Replenishing stock or taking finished product into the respective area Receiving Unload 50 of the day will be on one of the many types of forklift The remaining 50 will be a variety of warehouse duties skid wrapping palletizing using computers to check in product labeling boxes etc Medela Job Description Utilize forklifts pallets jacks RF scanners hand carts scales box cutters pallet wrappers and all equipment necessary to manage the replenishment of all warehouse areas following verbal and written department procedures work aids work orders work instructions operating and maintenance instructions and or procedure manuals Instructions Executes replenishment report per Instructions Understands how to interpret and identify warehouse locations and can perform all forms of product movements per Instructions Basic understanding and execution of inventory error resolution process following Instructions Understand and apply all process related data inputs on the company host computer system per Instructions Provides accurate time application and recording following Instructions."
            print("Test case #3: ", model.predict("3", job_title=title, job_description=description))
            # test case 4:
            title = ""
            description = "Summary Warehouse candidates will rotate weekly between the 4 primary roles: Replenishing Back end warehouse Receiving Unloading They rotate employees so that they don t get sick of doing 1 job and are always learning and doing new tasks Also it creates a more well rounded workforce and candidates feel more valuable having an array of skills Replenishing Back end 95 of the day is on one of the many types of forklift will be grabbing material and moving to requested locations Replenishing stock or taking finished product into the respective area Receiving Unload 50 of the day will be on one of the many types of forklift The remaining 50 will be a variety of warehouse duties skid wrapping palletizing using computers to check in product labeling boxes etc Medela Job Description Utilize forklifts pallets jacks RF scanners hand carts scales box cutters pallet wrappers and all equipment necessary to manage the replenishment of all warehouse areas following verbal and written department procedures work aids work orders work instructions operating and maintenance instructions and or procedure manuals Instructions Executes replenishment report per Instructions Understands how to interpret and identify warehouse locations and can perform all forms of product movements per Instructions Basic understanding and execution of inventory error resolution process following Instructions Understand and apply all process related data inputs on the company host computer system per Instructions Provides accurate time application and recording following Instructions."
            print("Test case #4: ", model.predict("4", job_title=title, job_description=description))
            # test case 5:
            title = "Warehouse Material Handler - 1st Shift"
            description = ""
            print("Test case #5: ", model.predict("5", job_title=title, job_description=description))
            # test case 6:
            title = "Warehouse Material Handler - 1st Shift"
            description = "Summary Warehouse candidates will rotate weekly between the 4 primary roles: Replenishing Back end warehouse Receiving Unloading They rotate employees so that they don t get sick of doing 1 job and are always learning and doing new tasks Also it creates a more well rounded workforce and candidates feel more valuable having an array of skills Replenishing Back end 95 of the day is on one of the many types of forklift will be grabbing material and moving to requested locations Replenishing stock or taking finished product into the respective area Receiving Unload 50 of the day will be on one of the many types of forklift The remaining 50 will be a variety of warehouse duties skid wrapping palletizing using computers to check in product labeling boxes etc Medela Job Description Utilize forklifts pallets jacks RF scanners hand carts scales box cutters pallet wrappers and all equipment necessary to manage the replenishment of all warehouse areas following verbal and written department procedures work aids work orders work instructions operating and maintenance instructions and or procedure manuals Instructions Executes replenishment report per Instructions Understands how to interpret and identify warehouse locations and can perform all forms of product movements per Instructions Basic understanding and execution of inventory error resolution process following Instructions Understand and apply all process related data inputs on the company host computer system per Instructions Provides accurate time application and recording following Instructions."
            print("Test case #6: ", model.predict("", job_title=title, job_description=description))
    elif len(sys.argv) == 4:  # given input data file
        process_type = sys.argv[1]
        input_datafile = sys.argv[2]
        output_file = sys.argv[3]
        if process_type.lower() == "--predict":
            if input_datafile and path.exists(input_datafile):
                start_time = datetime.datetime.now()
                model = OccupationModel()
                model.predict_df(input_datafile).to_parquet(output_file, index=False, engine='pyarrow')
                print("The prediction results have been successfully saved into the output file ", output_file)
                print('Time Taken:', (datetime.datetime.now() - start_time))
            else:
                print("The input data file doesn't exist!")
        else:
            print("The wrong arguments!")
    elif len(sys.argv) == 3 and sys.argv[1] == "--train":
        input_datafile = sys.argv[2]
        if input_datafile and path.exists(input_datafile):
            print("Start Occupation Model Training...")
            start_time = datetime.datetime.now()
            model = OccupationModel()
            model.train(input_datafile)
            print('Time Taken:', (datetime.datetime.now() - start_time))
        else:
            print("The input data file doesn't exist!")
    else:
        print("The wrong arguments!")
