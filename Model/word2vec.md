In this README, we present simply how to train a word embedding model and how to use the pre-trained word embedding (word2vec) model.

__Word Embedding Word2Vec with Gensim__

A word embedding is a form of representing words and documents using a dense vector representation. The position of a word within the vector space is learned from text and is based on the words that surround the word when it is used. Word embeddings can be trained using the input texts. One can read more about word embeddings [here](https://www.analyticsvidhya.com/blog/2017/06/word-embeddings-count-word2veec/) and [here](https://jalammar.github.io/illustrated-word2vec/).

__Word Embedding Model Training with Gensim__

Training a word embedding model can be easily done by executing following command at the folder where the 
```word2vec_model.py``` file locates:
```bash
python word2vec_model.py input_data_file.parquet
```
where ```input_data_file``` is the parquet data file for training.

__Word2Vec Pre-trained Model for Job Descriptions__

A word2vec (word embedding) model was trained with ~2.5 million job descriptions (from Cloudera production ```rf_job_description``` table).It was saved as a binary file and uploaded onto DSIA team site 
([Word2Vec_pretrained.zip](https://allegiscloud.sharepoint.com/teams/DataScienceandIndustryAnalytics/Shared%20Documents/General/Data%20Science%20Documentation%20(DSIA%20Internal%20Work)/Pre-trained%20Word%20Embedding%20(word2vec)%20Models/Job%20Descriptions%202.5M/Word2Vec_pretrained.zip)).

It is 300 dimensions (for each word) vectors and contain 381,161 words as vocabulary.

The following codes show how to use this pre-train model in a Python data science project to generate word embedding (word2vec) vectors for a given text or job description text column.


```python
# Step 1: import gensim word2vec library
from gensim.models import Word2Vec
```


```python
# Step 2: load model when needed so that this word2vec model doesn't need to be re-trained. 
# The string parameter in the load function is the path of model file.
model_w2v = Word2Vec.load('nnc_word2vec.bin')
print(model_w2v)
```


```python
# Optional: summarize vocabulary
word_vocabulary = list(model_w2v.wv.vocab)
print(word_vocabulary)
```


```python
# Step 3: define a function to get vectors
# Define a function to get average (mean) vectors for a job description based on pre-trained word2vec model
def get_mean_vectors(words):
    # remove out of vocabulary words
    words = [word for word in words if word in model_w2v.wv.vocab]
    if len(words) >= 1:
        return np.mean(model_w2v[words], axis=0)
    else:
        return []
```


```python
# Step 4: apply the model to generate vectors for a given text or job description string column
# Apply the function to generate 300 dimension vectors for each job description in the dataframe df.
# Assumming df has a column called job_description
df['job_description_vectors'] = df.apply(lambda row: get_mean_vectors(row.job_description), axis=1)

# Alternatively generate 300 dimension vector for a given text. For example,
sentence = ['analyst', 'development', 'sql', 'sdlc']
sentence_vectors = get_mean_vectors(sentence)
```
