"""
This module is to train a word2vec model with n dimensions and save it in a file.
Author: Ivan Chen
Date:   04/25/2020
Modification History:
- initial creation at 04/25/2020
"""
import pandas as pd
import numpy as np
import multiprocessing as mp
from gensim.models import Word2Vec
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
import re
from os import path
import pyarrow
import datetime
import sys
import logging
import warnings


logging.getLogger().setLevel(logging.ERROR)
warnings.filterwarnings("ignore", category=DeprecationWarning)
np.random.seed(42)
BLOCK_SIZE = 20000
COLUMN_NAME = "job_description"
OUTPUT_FILE = "nnc_word2vec.bin"


def lemma(x: list) -> list:
    """
    This defines lemmatization function by using NLTK WordNetLemmatizer function
    x: input list of words
    return: a list of lemmatized words
    """
    lemmatizer = WordNetLemmatizer()
    return [lemmatizer.lemmatize(w.lower(), pos='v') for w in x if (w != '') and (w is not None)]


class Word2vecTraining:
    def __init__(self):
        self.cpu_count = mp.cpu_count()
        self.stop_words = stopwords.words('english')

    def preprocess(self, df: pd.DataFrame) -> pd.DataFrame:
        if len(df) > 0:
            if COLUMN_NAME in df.columns:
                df = df[[COLUMN_NAME]]
                # remove any empty rows
                df = df.dropna()
                p = re.compile("[^a-zA-Z0-9]+")
                df[COLUMN_NAME] = df[COLUMN_NAME].apply(lambda x: p.sub(" ", str(x)))
                # Tokenizing
                df[COLUMN_NAME] = df[COLUMN_NAME].str.lower()
                df[COLUMN_NAME] = df[COLUMN_NAME].str.split(" ")
                # Lemmatization
                df[COLUMN_NAME] = df[COLUMN_NAME].apply(lambda x: lemma(x))
                return df
            else:
                print("The column name of", COLUMN_NAME, "doesn't exist in the input data set!")
                return pd.DataFrame()
        else:
            print("The input data set is empty!")
            return pd.DataFrame()

    def train(self, input_df: pd.DataFrame) -> Word2Vec:
        if len(input_df) > 0:
            sentences = input_df[COLUMN_NAME].tolist()
            # Train word2vec model
            model_w2v = Word2Vec(sentences, size=300, window=5, min_count=1, workers=self.cpu_count)
            return model_w2v
        else:
            print("Input data set is empty!")
            return Word2Vec()


def df_split(df: pd.DataFrame, block_size: int) -> list:
    """
    This function is to split the data frame equally
    df: input data frame
    block_size: the size of data block after splitting
    return: a list of small data frames after splitting
    """
    result = []
    if len(df) > 0:
        counter = (len(df) // block_size) + 1
        if counter > 0:
            result = np.array_split(df, counter)
        else:
            result.append(df)
    else:
        print("The input data frame doesn't exist!")
        result = []
    return result


def model_preprocess(df: pd.DataFrame) -> pd.DataFrame:
    """
    This function is a wrapper of level model prediction with a given small data frame
    df: the input small data frame
    return: the prediction data frame
    """
    if len(df) > 0:
        model = Word2vecTraining()
        result = model.preprocess(df)
        print("The pre-processing on data block of", len(df), "records has been finished!")
        return result
    else:
        return pd.DataFrame()


def main_process(input_data: str):
    if input_data and path.exists(input_data):
        nltk.download("stopwords")
        nltk.download('wordnet')
        nltk.download('omw')
        nltk.download('punkt')
        input_df = pd.read_parquet(input_data, engine='pyarrow')
        print("Start Training Data Pre-processing...")
        start_time = datetime.datetime.now()
        tmp = df_split(input_df, BLOCK_SIZE)
        pool = mp.Pool(mp.cpu_count())
        df_results = pool.map(model_preprocess, tmp)
        result_final = pd.concat(df_results)
        pool.close()
        pool.join()
        print("Start Word2Vec Model Training... ")
        print("The total amount of valid training data is: ", len(result_final))
        model = Word2vecTraining()
        model_w2v = model.train(result_final)
        # Save the trained model into output file
        model_w2v.save(OUTPUT_FILE)
        print("The trained word2vec model has been successfully saved into the file of", OUTPUT_FILE)
        print('Time Taken:', (datetime.datetime.now() - start_time))
    else:
        print("Input data file doesn't exist!")


if __name__ == "__main__":
    if len(sys.argv) == 1:
        input_file = input("Please give the training data (.parquet) file name with path: ")
        main_process(input_file)
    elif len(sys.argv) == 2:
        input_datafile = sys.argv[1]
        main_process(input_datafile)
    elif len(sys.argv) == 3 and sys.argv[1] == "--train":
        input_datafile = sys.argv[2]
        main_process(input_datafile)
    else:
        print("The wrong arguments!")


