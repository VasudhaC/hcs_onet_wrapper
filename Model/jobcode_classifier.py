# """
# This module is to provide O*NET-SOC job code predictions and Skill Specialty predictions
# Author: Vikranth Ranga, Alyssa Spitzer
# Date:   1/21/2021
# Modification History:
# - initial creation: 
# """

import numpy as np
import pandas as pd
import warnings
import flask
from flask import Flask, jsonify, abort, make_response, request
import json
import re
from google.cloud import automl_v1beta1 as automl
from datetime import datetime
from collections import defaultdict

from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value
from collections import defaultdict


warnings.filterwarnings("ignore", category=DeprecationWarning)


class jobcodeclassifier:
#     """
#     This class is the job code classifier model container that provides:
#     - Model Pre-Processing
#     - O*NET-SOC Code Classifications
#     - clean up functions
#     """    

    def __init__(self):
        # - onet_soc_labels.csv
        self.ONET_LOOKUP = pd.read_csv('Model/datasets/onet_soc_labels.csv').set_index('code', drop=False)
        self.ONET_LOOKUP = self.ONET_LOOKUP.rename(columns={'code':'onet_soc_code'})
        self.ONET_LOOKUP['label'] = self.ONET_LOOKUP['label'].astype(str)

        # automl objects
        self.client_options = dict(api_endpoint="us-central1-prediction-aiplatform.googleapis.com")
        self.client = aiplatform.gapic.PredictionServiceClient(client_options=self.client_options)
        self.endpoint = self.client.endpoint_path(project="acs-is-dsia-prod", location="us-central1", endpoint="7359339187960545280")
        self.parameters_dict = dict()
        self.parameters = json_format.ParseDict(self.parameters_dict, Value())
        print("init function was called")
            

    def model_pre_processing(self, title_input, description_input):
    #             """
    #             This function is to perform necessary pre-processing model activities
    #             :param: input dataframe of work history blocks
    #             :return: dataframe of work history blocks 
    #             """    
        if len(title_input) + len(description_input) >0:
            # track timing of function
            model_pre_processing_start = datetime.now()

            title = title_input
            description = description_input
            
            # remove special characters from title and description
#             title = title.astype(str)
#             description = description.astype(str)

            # this function is to clean the text to remove all special characters
            def text_clean(x):
                p = re.compile("[^a-zA-Z0-9]+")
                if len(x)>4:
                    x = p.sub(" ",x)
                    return x
                    if len(x)<4:
                        x = "None"
#                     print(x)
                    return x
                else:
                    x = "None"
#                     print(x)
                    return x
                        
            cleaned_title = text_clean(title)
            cleaned_description = text_clean(description)    
            
            # print("Model Pre-processing Completed in:", datetime.now() - model_pre_processing_start)
            return cleaned_title, cleaned_description
        else:
            print("O*NET-SOC Model Pre-processing Activities not Completed")
            cleaned_title = "None"
            cleaned_description = "None"  
            return cleaned_title,cleaned_description
    
        
    def model_predict(self, title_input, description_input):
    #             """
    #             This function is to use the O*NET-SOC Model to predict on title and description
    #             :param: input title and description
    #             :return: onet soc code, description, confidence
    #             """    
        if len(title_input) + len(description_input) >0:
            model_start = datetime.now()
            
            title = title_input
            description = description_input
            
            title, description = self.model_pre_processing(title, description)
            
            # this function is to lookup the correct formatted onet soc code and descriptions
            def get_values(onet_soc_code):
                onet = self.ONET_LOOKUP[self.ONET_LOOKUP['label'] == onet_soc_code]
                code = onet.values[0][1]
                description = onet.values[0][2]
                return code, description

            # this function is to select the appropriate onet soc code from the list of model responses
            # and also to provide added clean up where there is a low confidence on the model results
            def cleanup_response(response):
                predictions = response.predictions
                df = pd.DataFrame.from_dict(predictions)   
                filt_resp=defaultdict()
                for x in range(len(df)):
                    classes = df.classes[x]
                    scores = list(df.scores[x])
                    confidence = max(df.scores[x])
                    status = 'OK'
                    message = 'Successful'
                    if confidence >= 0.2:
                        value = scores.index(confidence)
                        onet_soc = classes[value]
                    else:
                        onet_soc = '0'
                        confidence = 0.0
                    code, description = get_values(onet_soc)
                    filt_resp[x]={"status":status,"message":message,"confidence":confidence,"onet_soc_code":code, "onet_soc_description": description}
                return filt_resp.get(0)

            
            # call onet-soc model
            instances = [json_format.ParseDict(dict(title=title,description=description), Value())]
            try:
                response = self.client.predict(endpoint=self.endpoint, instances=instances, parameters=self.parameters)
                message = "Successful"
            except Exception as e:
                print(e)
                message = "Unsuccessful - O*NET-SOC Model unable to be completed"

            if response:
                cleaned_resp=cleanup_response(response)
            else:
                cleaned_resp = {'status':'OK','message':message,'confidence': 0.0,'onet_soc_code':'00-0000.00', 'onet_soc_description':'Unknown'}
                print("Model unable to be called")

            # print("Model Completed in :",datetime.now() - model_start)
            return cleaned_resp
        else:
            cleaned_resp = {'status':'OK','message':'Unsuccessful - O*NET-SOC model has invalid inputs','confidence': 0.0,'onet_soc_code':'00-0000.00', 'onet_soc_description':'Unknown'}
            print("O*NET-SOC Model Activities not Completed")
            
            return cleaned_resp
        
    
        