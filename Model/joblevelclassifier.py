"""
This module is to provide some functions on experience level predictions
Author: Ivan Chen
Date:   04/25/2020
Modification History:
- initial creation at 04/25/2020
"""
from __future__ import division, print_function
import sys
from os import path
import pandas as pd
import numpy as np
import re
import logging
import warnings


logging.getLogger().setLevel(logging.ERROR)
# disable warning message show-up
warnings.filterwarnings("ignore", category=DeprecationWarning)

dir_path = path.dirname(path.realpath(__file__))
# noinspection SpellCheckingInspection
LEVEL_TERM_FILE = dir_path + '/datasets/level_terms.csv'
# noinspection SpellCheckingInspection
LEVEL_CUTOFFS_FILE = dir_path + '/datasets/aa_all_hcs_level_cutoffs.csv'


def regex_find(text: str, term: str) -> str:
    """
    This function is to search and then return found terms from input text.
    :param text: a text/string where the given term can be found from
    :param term: the Regex term that this function is trying to look for
    :return: the sub-string that contains the term if found, otherwise, return None
    """
    if text and term:
        x = re.search(term, text.lower())
        if x is not None:
            return x.group()
        else:
            return ""
    else:
        return ""


def level_prediction(jd_title: str, title_level_info: list, jd_description: str, jd_level_info: list) -> str:
    """
    This function is to predict the level (0,1,2,3) based on input job title and job description.
    :param jd_title: input job title string
    :param title_level_info: pre-loaded level terms for job title
    :param jd_description: input job description string
    :param jd_level_info: pre-loaded level terms for job description
    :return: predicted level combining level confidence value as a whole string with comma limited
    """
    # find all matched terms in the title
    if jd_title and len(jd_title) > 0:
        term_probability = [term for term in title_level_info if regex_find(jd_title, term[0]) != ""]
    else:
        term_probability = []

    # find all matched terms in the job description
    if jd_description and len(jd_description) > 0:
        tmp_prob = [term for term in jd_level_info if regex_find(jd_description, term[0]) != ""]
        term_probability.extend(tmp_prob)

    if term_probability and len(term_probability) != 0:
        level_1_prob = max([item[1] for item in term_probability if len(item) > 0])
        level_2_prob = max([item[2] for item in term_probability if len(item) > 0])
        level_3_prob = max([item[3] for item in term_probability if len(item) > 0])
        # get top 1 and top 2 max probabilities
        term_probability = [level_1_prob, level_2_prob, level_3_prob]
        max_prob_1 = max(term_probability)
        max_index_1 = int(np.argmax(term_probability))
        term_probability.pop(max_index_1)
        max_prob_2 = max(term_probability)

        # return the level with its max probability
        if max_prob_1 == max_prob_2:
            return "0,0"
        else:
            return "".join(str(max_index_1 + 1)) + "," + str(max_prob_1)
        # # Alyssa Spitzer updated 5/11/2020 to separate out level code
        # # review with team to if two probs are the same and not 0
        # if max_prob_1 == max_prob_2 and max_prob_1 == 0:
        #     return "0,0"
        # elif max_prob_1 == max_prob_2:
        #     return "".join(str(max_index_1 + 1)) + "," + str(max_prob_1)
        # else:
        #     return "".join(str(max_index_1 + 1)) + "," + str(max_prob_1)
    else:
        return "0,0"


def trash_can_flag(hcs: str, level: int, level_confidence: float, bill_rate: float, entry_level_min: float, entry_level_max: float, intermediate_level_min: float, intermediate_level_max: float, expert_level_min: float, expert_level_max: float) -> str:
    result_flag = "normal"  # {outlier, unknown, normal}
    final_level = 0
    # if (hcs is None) or (hcs == '00-000') or (hcs_confidence < 0.2):
    if (hcs is None) or (hcs == '00-000'):
        result_flag = "unknown"
    if (level is None) or (level <= 0) or (level_confidence < 0.3):
        result_flag = "unknown"
    if (bill_rate is not None) and (bill_rate > 0) and (level > 0):
        if (bill_rate >= entry_level_min) and (bill_rate <= entry_level_max):
            final_level = 1
        elif (bill_rate >= entry_level_min) and (bill_rate <= entry_level_max * 1.1) and (level_confidence >= 0.8):
            final_level = 1
        elif (bill_rate >= intermediate_level_min) and (bill_rate <= intermediate_level_max):
            final_level = 2
        elif (bill_rate >= intermediate_level_min) and (bill_rate <= intermediate_level_max * 1.1) and (level_confidence >= 0.8):
            final_level = 2
        elif (bill_rate >= expert_level_min) and (bill_rate <= expert_level_max):
            final_level = 3
        elif (bill_rate >= expert_level_min) and (bill_rate <= expert_level_max * 1.1) and (level_confidence >= 0.8):
            final_level = 3
        if level != final_level:
            result_flag = "outlier"

    return "".join(result_flag) + "," + str(final_level)


class LevelModel:
    """
    This class is the level model container that provide
    - level model prediction (real-time & batch modes)
    - final level process based on bill rates and level cut offs
    """
    def __init__(self):
        """
        Load support data into memory
        Initialize variables
        """
        try:
            np.random.seed(42)
            terms_df = pd.read_csv(LEVEL_TERM_FILE, header='infer')
            jd_terms_df = terms_df.query("Location != 'title'")
            title_terms_df = terms_df.query("Location != 'description'")
            self.jd_level_info = jd_terms_df.to_numpy()
            self.title_level_info = title_terms_df.to_numpy()
            self.df_cutoffs = pd.read_csv(LEVEL_CUTOFFS_FILE, header='infer')
        except ValueError or IOError:
            print('An error occurred trying to initialize the level model!')
            sys.exit()

    def predict_df(self, datafile: str, data_frame=None) -> pd.DataFrame:
        """
        This function is to predict level information for the given data from a file
        :param data_frame: input pandas DataFrame object
        :param datafile: the datafile name and path
        :return: the data frame containing the level prediction and level confidence columns
        """
        if data_frame is not None and len(data_frame) > 0:
            df = data_frame
        elif datafile and path.isfile(datafile):
            # noinspection SpellCheckingInspection
            df = pd.read_parquet(datafile, engine='pyarrow')
        else:
            df = None
        if df is not None and len(df) > 0:
            # noinspection SpellCheckingInspection
            if ('job_title' in df.columns) and ('job_description' in df.columns):
                df['predictions'] = df.apply(lambda row: level_prediction(str(row.job_title), self.title_level_info, str(row.job_description), self.jd_level_info), axis=1)
                df[['experience_level', 'experience_level_confidence']] = df.predictions.str.split(",", expand=True)
                # print("This level data prediction thread has been finished.")
                return df.drop(['predictions'], axis=1)
            else:
                return pd.DataFrame()  # return an empty data frame
        else:
            return pd.DataFrame()  # return an empty data frame

    def predict(self, job_title: str, job_description: str) -> str:
        """
        This function is to predict level information based on the given a job title and job description
        :param job_title: input job title string
        :param job_description: input job description string
        :return: predicted level combining level confidence value as a whole string with comma limited
        """
        if job_title and job_description:
            return level_prediction(job_title, self.title_level_info, job_description, self.jd_level_info)
        else:
            return "0,0"

    def final_process(self, datafile: str, data_frame=None) -> pd.DataFrame:
        """
        This function is to predict level information for the given data from a file
        :param data_frame: input pandas DataFrame object
        :param datafile: the datafile name and path
        :return: the data frame containing the level prediction and level confidence columns
        """
        if data_frame is not None and len(data_frame) > 0:
            input_data = data_frame
        elif datafile and path.exists(datafile):
            # noinspection SpellCheckingInspection
            input_data = pd.read_parquet(datafile, engine='pyarrow')
        else:
            input_data = None
        if input_data is not None and len(input_data) > 0:
            if ('country_code' in input_data.columns) and ('occupation' in input_data.columns) and ('occupation_confidence' in input_data.columns) and ('experience_level' in input_data.columns) and ('experience_level_confidence' in input_data.columns) and ('bill_rate' in input_data.columns):
                df_new = pd.merge(input_data, self.df_cutoffs, how='left', left_on=['country_code', 'occupation'], right_on=['country_code', 'hcs'])
                df_new["occupation"].fillna("00-000", inplace=True)
                df_new["experience_level"].fillna(0, inplace=True)
                df_new.fillna(0, inplace=True)
                df_new['trash_can'] = df_new.apply(lambda row: trash_can_flag(row.hcs, row.experience_level, row.experience_level_confidence, row.bill_rate, row.entry_level_min, row.entry_level_max, row.intermediate_level_min, row.intermediate_level_max, row.expert_level_min, row.expert_level_max), axis=1)
                df_new[['flag', 'final_level']] = df_new.trash_can.str.split(",", expand=True)
                return df_new[['req_guid', 'country_code', 'occupation', 'occupation_confidence', 'bill_rate', 'experience_level', 'experience_level_confidence', 'flag', 'final_level']]
            else:
                print('Missing Columns in input data! Level final process requires columns including req_guid, country_code, occupation, occupation_confidence, experience_level, experience_level_confidence, and bill_rate.')
                return pd.DataFrame()  # return an empty data frame


if __name__ == "__main__":
    if len(sys.argv) == 2:
        if sys.argv[1] == "--test":
            model = LevelModel()
            # test case 1:
            title_str = "Senior Front End Developer"
            # noinspection SpellCheckingInspection,SpellCheckingInspection,PyPep8
            description_str = "Product Development - organization Pro Connect Group PCG They are looking for creative problem solvers with a passion for innovation Responsibilities Significant experience developing front end applications Significant experience developing automated tests for your features so you can get back to the fun part Hands on coding experience producing high quality experiences Resolve defects bugs during QA testing pre production production and post release patches Contribute to a scrum team focused on providing the essential benefit to customers Work with cross functional teams to ensure effective definition design and implementation Work closely with Experience Designers to understand usability information architecture Work closely with the engineering team to build the UI screens Qualifications 3 to 8 years of experience developing web applications Programming experience in several technologies including JavaScript ES6 HTML5 CSS3 Webpack Babel and SASS Working experience with frameworks and libraries such as Angular JS React JS Redux VUE jsS etc Must be able to hand code javascript Knowledge of web application development fundamentals RESTful Web Services would be a plus Knowledge JavaScript testing frameworks Test Driven Development TDD experience a plus Hands on experience creating responsive web applications using modern CSS frameworks Expert UI skills but able to understand and work in the business logic layer when needed Ability to establish priorities and work independently on multiple tasks Excellent problem solving skills Knowledge of software development methodologies and practices especially Agile iterative development processes Preferred Experience Experience developing maintaining and innovating large scale consumer facing web or mobile applications Familiar with the development challenges inherent with highly scalable and available web applications Always Be Learning Experience with open source technologies if no practical work experience with cutting edge front end technology you re prototyping and or researching the upcoming technologies and solutions Experience with various modern web frameworks Passion for creating a better user experience through understanding of usability information architecture use case development design aesthetic Experience with web analytics A B testing data visualization graphing and charting."
            print("Test case #1: ", model.predict(job_title=title_str, job_description=description_str))
            # test case 2:
            title_str = "Junior Warehouse Material Handler - 1st Shift"
            # noinspection SpellCheckingInspection,SpellCheckingInspection,PyPep8
            description_str = "Summary Warehouse candidates will rotate weekly between the 4 primary roles: Replenishing Back end warehouse Receiving Unloading They rotate employees so that they don t get sick of doing 1 job and are always learning and doing new tasks Also it creates a more well rounded workforce and candidates feel more valuable having an array of skills Replenishing Back end 95 of the day is on one of the many types of forklift will be grabbing material and moving to requested locations Replenishing stock or taking finished product into the respective area Receiving Unload 50 of the day will be on one of the many types of forklift The remaining 50 will be a variety of warehouse duties skid wrapping palletizing using computers to check in product labeling boxes etc Medela Job Description Utilize forklifts pallets jacks RF scanners hand carts scales box cutters pallet wrappers and all equipment necessary to manage the replenishment of all warehouse areas following verbal and written department procedures work aids work orders work instructions operating and maintenance instructions and or procedure manuals Instructions Executes replenishment report per Instructions Understands how to interpret and identify warehouse locations and can perform all forms of product movements per Instructions Basic understanding and execution of inventory error resolution process following Instructions Understand and apply all process related data inputs on the company host computer system per Instructions Provides accurate time application and recording following Instructions."
            print("Test case #2: ", model.predict(job_title=title_str, job_description=description_str))
    elif len(sys.argv) == 4:  # given input data file
        process_type = sys.argv[1]
        if process_type.lower() == "--predict":
            data_file = sys.argv[2]
            output_file = sys.argv[3]
            if data_file and path.exists(data_file):
                model = LevelModel()
                tmp = model.predict_df(data_file)
                if tmp is not None and len(tmp) > 0:
                    # noinspection SpellCheckingInspection
                    tmp.to_parquet(output_file, index=False, engine='pyarrow')
                    print("The prediction results have been successfully saved into the file of", output_file)
                else:
                    print("Nothing saved due to empty result!")
            else:
                print("The input data file doesn't exist!")
        elif process_type.lower() == "--final-level":
            data_file = sys.argv[2]
            output_file = sys.argv[3]
            if data_file and path.exists(data_file):
                model = LevelModel()
                tmp = model.final_process(data_file)
                if tmp is not None and len(tmp) > 0:
                    # noinspection SpellCheckingInspection
                    tmp.to_parquet(output_file, index=False, engine='pyarrow')
                    print("The prediction results have been successfully saved into the file of", output_file)
                else:
                    print("Nothing saved due to empty result!")
            else:
                print("The input data file doesn't exist!")
    else:
        print("The wrong arguments!")
