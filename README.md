# Model Report
This document provides necessary details of the Wrapper API which would be leveraged for opportunity data classification.

[Wrapper API Teams Link](https://teams.microsoft.com/_#/files/General?threadId=19%3A0b0a9c3b160f4fba9eba29909702094d%40thread.skype&ctx=channel&context=Opportunity%2520Wrapper%2520API&rootfolder=%252Fteams%252FDataScienceandIndustryAnalytics%252FShared%2520Documents%252FGeneral%252FData%2520Science%2520Documentation%2520(DSIA%2520Internal%2520Work)%252FOpportunity%2520Data%2520Classification%252FOpportunity%2520Wrapper%2520API)

## Business Background
DSIA supports multiple APIs that are leveraged for opportunity data classification. As part of streamlining these calls into a more efficient process, DSIA has built a ‘wrapper’ which wraps four API calls into one. 

Within these three API calls there are 5 models and one data lookup service:

- Occupation HCS Model 
- Level HCS Model
- RVT Look-up
- Job Code O*NET -SOC Model
- Skillset Model
- Skill Specialty Model

This new single API call will take an inbound request, call sub-models and build a final response. 

## Contacts
- Vikranth Ranga, Alyssa Spitzer
