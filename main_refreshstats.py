# Import the necessary packages
import logging
from flask import Flask, jsonify, abort, make_response, request
import pandas as pd 
import nltk
import pandas as pd
import snowflake.connector
from snowflake.connector import ProgrammingError
import os
import re
from collections import defaultdict
import zipfile
import json
import datetime
import jwt
from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value
from google.cloud import storage

app = Flask(__name__)
nltk.download("stopwords")
nltk.download('wordnet')
nltk.download('omw')
nltk.download('punkt')

"""
This section is to read the necessary lookup files
"""
# Read snowflake table from GCS bucket
# Configured this environment variable via app.yaml file
CLOUD_STORAGE_BUCKET = os.environ['CLOUD_STORAGE_BUCKET']
bucket_name = "rpt_rvt_stats"
#df_rvt_stats = pd.read_csv('gs://'+CLOUD_STORAGE_BUCKET+'/rpt_rvt_stats.csv')


"""
This section is to install the specific Model codes
"""
# Model: HCS Occupation, Experience Level Classifier, and RVT Data Lookup
from Model.occupationclassifier import OccupationModel
from Model.joblevelclassifier import LevelModel
from Model.RVT_lookup import RVT_lookup
hcs_model = OccupationModel()
level_model = LevelModel()
RVT_lookup = RVT_lookup()

# Model: O*NET-SOC Occupation Classifier
from Model.jobcode_classifier import jobcodeclassifier
job_code = jobcodeclassifier()

# Model: Skill Specialty Classifier
from Model.skill_specialty import skillclassifier
skill_specialty = skillclassifier()

# Model: Skillset Classifier
from Model.skillset import skillset
skillset_model = skillset()

# Model: Functional Classifier
from Model.functional import functionalclassifier
functional_model = functionalclassifier()

#global variables set for rvt stats refresh check
# global last_modified_date
storage_client = storage.Client()
bucket = storage_client.get_bucket('rpt_rvt_stats')
blob = bucket.get_blob('rpt_rvt_stats_02.csv')
# last_modified_date = format(blob.updated)[0:10]
last_modified_date = format(blob.updated)[0:16]
print("last modified date of file:", last_modified_date)
#last_modified_date = check_update_date(gcs_csv_path)

#global df_rvt_stats
df_rvt_stats = pd.read_csv('gs://'+CLOUD_STORAGE_BUCKET+'/rpt_rvt_stats_02.csv')

# global updated_today
updated_today = True

# global count
# count = 0

# global refreshed_date
refreshed_date = datetime.datetime.today().strftime('%Y-%m-%d-%H-%M')

# REST API usage info
@app.route('/')
def index():
    return make_response(jsonify({'method':'POST', 'path':'Call /predict for the HCS occupation, experience level, RVT rate detail, O*NET-SOC, skill specialty, skillset prediction functionality.'}), 200)

# setup error handling
@app.errorhandler(404)
def page_not_found(error):
    return make_response(jsonify({'error':'Not found'}), 404)


# REST API for real-time proven track record predictions.
@app.route('/predict', methods=['POST'])
def create_task():
    #CLOUD_STORAGE_BUCKET = os.environ['CLOUD_STORAGE_BUCKET']
    #df_rvt_stats = pd.read_csv('gs://'+CLOUD_STORAGE_BUCKET+'/rpt_rvt_stats.csv')
    print("file location",  os.environ['CLOUD_STORAGE_BUCKET'] )
    global updated_today
    global refreshed_date
    global df_rvt_stats
    global last_modified_date
    if not updated_today:
        storage_client = storage.Client()
        bucket = storage_client.get_bucket('rpt_rvt_stats')
        blob = bucket.get_blob('rpt_rvt_stats_02.csv')
        current_modified_date = format(blob.updated)[0:16]
        print("second modified date of file:", current_modified_date)
        refreshed_date = datetime.datetime.today().strftime('%Y-%m-%d-%H-%M')
        print("refreshed date :", refreshed_date)
        updated_today = True
        if last_modified_date != current_modified_date: 
            df_rvt_stats = pd.read_csv('gs://'+CLOUD_STORAGE_BUCKET+'/rpt_rvt_stats_02.csv')
            last_modified_date = current_modified_date
    else:
        if refreshed_date != datetime.datetime.today().strftime('%Y-%m-%d-%H-%M'):
            updated_today = False
            print("refresh date and current time are same")
            print("if part in else part:", updated_today)
    if not request.json:
        abort(400)
    else:
        # get opportunity detail
        # id = request.json.get('id',"")
        id = request.json.get('id', "id_unknown")
        #print("id is:", id)
        req_job_title__c = request.json.get('req_job_title__c',"")
        req_job_description__c = request.json.get('req_job_description__c',"")
        req_qualification__c = request.json.get('req_qualification__c',"")
        enterprisereqskills__c = request.json.get('enterprisereqskills__c',"")
        req_skill_details__c = request.json.get('req_skill_details__c',"")
        req_opco_code__c = request.json.get('req_opco_code__c',"")
        suggested_job_titles__c = request.json.get('suggested_job_titles__c',"")
        metro_area = request.json.get('metro_area', "National")
        country_code = request.json.get('country_code', "USA")

        # call all wrapper models individually
        # Model: HCS Occupation, Experience Level Classifier, and RVT Data Lookup
        rvt_start = datetime.datetime.now()

        # call the hcs and experience level models
        try:
            hcs, hcs_desc, hcs_prob, hcs_group = hcs_model.predict(opportunity_id=id, job_title=req_job_title__c, job_description=req_job_description__c).split(",") 
            level, level_prob = level_model.predict(job_title=req_job_title__c, job_description=req_job_description__c).split(",")
        except Exception as e:
            print("HCS and/or Level Model failure: ", e)
            hcs = "00-000"
            hcs_desc = "0"
            hcs_prob = "0"
            hcs_group = "0"
            level = "0"
            level_prob = "0"
            hcs_message = "Unsuccessful - HCS and/or Experience Level Model unable to be called"

        # call RVT lookup function
        try:
            rvt_output = RVT_lookup.lookup(hcs, hcs_desc, hcs_prob, hcs_group, level, level_prob, metro_area, country_code, df_rvt_stats)
        except Exception as e:
            print("RVT lookup failure: ", e)
            hcs_message = "Unsuccessful - RVT lookup unable to be completed"
            rvt_output = {
                "message": hcs_message,
                "status": "OK",
                "Occupation_Group": "00-000",
                "Occupation": "0",
                "Occupation_confidence_value": "0",
                "Experience_level": "0",
                "Experience_level_confidence_value": "0",
                "RVT_detail" : "Unknown"
                }

        # verify output is available
        if rvt_output:
            rvt_response = rvt_output
        else:
            rvt_response = {
                "message": "Unsuccessful",
                "status": "OK",
                "Occupation_Group": "00-000",
                "Occupation": "0",
                "Occupation_confidence_value": "0",
                "Experience_level": "0",
                "Experience_level_confidence_value": "0",
                "RVT_detail" : "Unknown"
                }

        rvt_end = datetime.datetime.now()
        print("RVT time: ", rvt_end-rvt_start)



        # Model: O*NET-SOC Occupation Classifier
        onet_start = datetime.datetime.now()
        
        # call job code model function
        try:
            onet_output = job_code.model_predict(req_job_title__c, req_job_description__c)
        except Exception as e:
            print("O*NET-SOC Job Code Model failure: ", e)
            onet_message = "Unsuccessful - O*NET-SOC Model unable to be completed"
            onet_output = {
                'status':'OK',
                'message':onet_message,
                'confidence': 0.0,
                'onet_soc_code':'00-0000.00', 
                'onet_soc_description':'Unknown'
                }

        # verify job code output is available
        if onet_output:
            onet_response = onet_output
        else:
            onet_response = {
                'status':'OK',
                'message':"Unsuccessful",
                'confidence': 0.0,
                'onet_soc_code':'00-0000.00', 
                'onet_soc_description':'Unknown'
                }
        
        onet_end = datetime.datetime.now()
        print("onet time: ", onet_end-onet_start)

        # Model: Skill Specialty Classifier
        skill_specialty_start = datetime.datetime.now()

        # call skill specialty model
        try:
            skill_specialty_output = skill_specialty.model_predict(req_job_title__c, req_job_description__c)
        except Exception as e:
            print("Skill Specialty failure: ", e)
            skill_specialty_message = "Unsuccessful - Skill Specialty Model unable to be completed"
            skill_specialty_output = {
                "label": "Unknown",
                "message": skill_specialty_message,
                "score": "0",
                "status": "OK"
                } 
        
        # verify skill specialty output is available
        if skill_specialty_output:
            skill_specialty_response = skill_specialty_output
        else:
            skill_specialty_response = {
                "label": "Unknown",
                "message": "Unsuccessful",
                "score": "0",
                "status": "OK"
                }    

        skill_specialty_end = datetime.datetime.now()    
        print("skill specialty time: ", skill_specialty_end-skill_specialty_start)

        # Model: Skillset Classifier
        skillset_start = datetime.datetime.now()  

        # call skillset model
        try:
            skillset_output = skillset_model.predict(
                id,
                req_job_title__c,
                req_job_description__c,
                req_qualification__c,
                enterprisereqskills__c,
                req_skill_details__c,
                req_opco_code__c,
                suggested_job_titles__c
                )
        except Exception as e:
            print("Skillset model failure: ", e)
            skillset_message = "Unsuccessful - Skillset Model unable to be completed"
            skillset_output = {
                "message": skillset_message,
                "status": "OK",
                "skillset_classifier":{}
                }

        # verify output is available
        if skillset_output:
            skillset_response = skillset_output
        else:
            skillset_response = {
                "message": "Unsuccessful",
                "status": "OK",
                "skillset_classifier": {}
                }

        # Model: Functional Classifier
        functional_start = datetime.datetime.now()  

        # call functional model
        onet_code = onet_response.get("onet_soc_code")
        try:
            functional_output = functional_model.predict(onet_code)
        except Exception as e:
            print("Functional model failure: ", e)
            functional_message = "Unsuccessful - Functional Model unable to be completed"
            functional_output = {
                "message": functional_message,
                "status": "OK",
                "functional":"Unknown"
                }

        # verify output is available
        if functional_output:
            functional_response = functional_output
        else:
            functional_response = {
                "message": "Unsuccessful",
                "status": "OK",
                "functional":"Unknown"
                }

        functional_end = datetime.datetime.now()  
        print("functional time: ", functional_end-functional_start)


        # set up responses to return all wrapper calls
        response = {
            "status": "OK",
            "message": "Successful",
            "id": id,
            "RVT": rvt_response,
            "onet_job_code": onet_response,
            "skill_specialty": skill_specialty_response,
            "skillset": skillset_response,
            "functional":  functional_response  
        }
        
    return jsonify(response)

if __name__ == '__main__':
    app.run()